package ru.fnight.tablechatview.controllers

import javafx.collections.FXCollections
import javafx.css.PseudoClass
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.scene.control.Alert
import javafx.scene.control.TableColumn
import javafx.scene.control.TableRow
import javafx.scene.control.TableView
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.control.cell.TextFieldTableCell
import javafx.util.Callback
import javafx.util.StringConverter
import ru.fnight.tablechatview.domains.User
import java.sql.Connection
import java.sql.SQLException
import java.sql.Statement
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.LocalDateTime

class UsersController {
    lateinit var usersTable: TableView<User>
    lateinit var userNameColumn: TableColumn<User, String>
    lateinit var userRegColumn: TableColumn<User, Timestamp>
    lateinit var userPwdColumn: TableColumn<User, String>

    lateinit var connection: Connection

    @FXML
    private fun initialize() {
        usersTable.isEditable = true

        //login
        userNameColumn.cellValueFactory = PropertyValueFactory<User, String>("login")
        userNameColumn.cellFactory = TextFieldTableCell.forTableColumn()
        userNameColumn.onEditCommit = EventHandler { event ->
            onChangeName(event)
        }

        //registration
        userRegColumn.cellValueFactory = PropertyValueFactory<User, Timestamp>("registration")
        userRegColumn.cellFactory = TextFieldTableCell.forTableColumn<User, Timestamp>(
                object : StringConverter<Timestamp>() {
                    override fun toString(obj: Timestamp): String {
                        return SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(obj)
                    }

                    override fun fromString(string: String): Timestamp {
                        return try {
                            Timestamp.valueOf(string)
                        } catch (e: IllegalArgumentException) {
                            Alert(Alert.AlertType.ERROR, "Неверный формат даты").show()
                            Timestamp.valueOf(LocalDateTime.now())
                        }
                    }
                }
        )
        userRegColumn.onEditCommit = EventHandler { event ->
            onChangeReg(event)
        }

        //pwd
        userPwdColumn.cellValueFactory = PropertyValueFactory<User, String>("pwd")
        userPwdColumn.cellFactory = TextFieldTableCell.forTableColumn()
        userPwdColumn.onEditCommit = EventHandler { event ->
            onChangePwd(event)
        }

        usersTable.rowFactory = Callback { param ->
            val row = TableRow<User>()
            row.itemProperty().addListener { observable, oldValue, newValue ->
                if (newValue != null) {
                    if (newValue.id <= 0) {
                        row.pseudoClassStateChanged(PseudoClass.getPseudoClass("new"), true)
                    } else {
                        row.pseudoClassStateChanged(PseudoClass.getPseudoClass("new"), false)
                    }
                }
            }
            row
        }
    }

    private fun onChangeName(event: TableColumn.CellEditEvent<User, String>) {
        val user = event.tableView.items[event.tablePosition.row]
        val oldValue = user.login
        val newValue = event.newValue
        if (newValue.isBlank()) {
            Alert(Alert.AlertType.ERROR, "Необходимо ввести не пустое имя пользователя").show()
            user.login = oldValue
            usersTable.refresh()
            return
        }
        user.login = newValue
        try {
            saveChangedUser(user)
        } catch (e: SQLException) {
            Alert(Alert.AlertType.ERROR, "Необходимо ввести уникальное имя").show()
            user.login = oldValue
            usersTable.refresh()
        }
    }

    private fun onChangePwd(event: TableColumn.CellEditEvent<User, String>) {
        val user = event.tableView.items[event.tablePosition.row]
        val oldValue = user.pwd
        user.pwd = event.newValue
        try {
            saveChangedUser(user)
        } catch (e: SQLException) {
            Alert(Alert.AlertType.ERROR, e.message).show()
            user.pwd = oldValue
            usersTable.refresh()
        }
    }

    private fun onChangeReg(event: TableColumn.CellEditEvent<User, Timestamp>) {
        val user = event.tableView.items[event.tablePosition.row]
        val oldValue = user.registration
        user.registration = event.newValue
        try {
            saveChangedUser(user)
        } catch (e: SQLException) {
            Alert(Alert.AlertType.ERROR, e.message).show()
            user.registration = oldValue
            usersTable.refresh()
        }
    }

    @Throws(SQLException::class)
    private fun saveChangedUser(user: User) {
        if (user.id == -1L) {
            val sql = "insert into users (login, registration, pwd) values (?, ?, ?)"
            val statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
            statement.setString(1, user.login)
            statement.setTimestamp(2, user.registration)
            statement.setString(3, user.pwd)
            statement.execute()
            val keys = statement.generatedKeys
            if (keys.next()) {
                user.id = keys.getLong(1)
            }
        } else {
            val sql = "update users set login = ?, registration = ?, pwd = ? where id = ?"
            val statement = connection.prepareStatement(sql)
            statement.setString(1, user.login)
            statement.setTimestamp(2, user.registration)
            statement.setString(3, user.pwd)
            statement.setLong(4, user.id)
            statement.execute()
        }
        usersTable.refresh()
    }

    fun reloadUsers(users: ArrayList<User>) {
        usersTable.items = FXCollections.observableArrayList(users)
        usersTable.refresh()
    }

    fun deleteUser(actionEvent: ActionEvent) {
        val user = usersTable.selectionModel.selectedItem ?: return
        if (user.id != -1L) {
            val sql = "delete from users where id = ?"
            val statement = connection.prepareStatement(sql)
            statement.setLong(1, user.id)
            try {
                statement.execute()
            } catch (e: SQLException) {
                Alert(Alert.AlertType.ERROR,
                        "Невозможно удалить пользователя, потому что у него есть зависимые объекты").show()
                return
            }
        }
        usersTable.items.remove(usersTable.selectionModel.selectedItem)
        usersTable.refresh()
    }

    fun addNewUser(actionEvent: ActionEvent) {
        usersTable.items.add(User(-1L, "Новый пользователь", Timestamp.valueOf(LocalDateTime.now()), ""))
        usersTable.refresh()
    }
}
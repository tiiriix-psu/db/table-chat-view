package ru.fnight.tablechatview.controllers

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.FXCollections
import javafx.css.PseudoClass
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.geometry.Pos
import javafx.scene.control.Alert
import javafx.scene.control.TableColumn
import javafx.scene.control.TableRow
import javafx.scene.control.TableView
import javafx.scene.control.cell.CheckBoxTableCell
import javafx.scene.control.cell.ComboBoxTableCell
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.control.cell.TextFieldTableCell
import javafx.util.Callback
import javafx.util.StringConverter
import ru.fnight.tablechatview.domains.Chat
import ru.fnight.tablechatview.domains.Session
import ru.fnight.tablechatview.domains.User
import java.sql.Connection
import java.sql.SQLException
import java.sql.Statement
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.LocalDateTime

class ChatsController {

    lateinit var chatsTable: TableView<Chat>
    lateinit var chatNameColumn: TableColumn<Chat, String>
    lateinit var chatCreationColumn: TableColumn<Chat, Timestamp>
    lateinit var chatOwnerColumn: TableColumn<Chat, User>
    lateinit var chatDeletedColumn: TableColumn<Chat, Boolean>

    lateinit var connection: Connection
    var users = ArrayList<User>()

    @FXML
    private fun initialize() {
        chatsTable.isEditable = true

        //name
        chatNameColumn.cellValueFactory = PropertyValueFactory<Chat, String>("name")
        chatNameColumn.cellFactory = TextFieldTableCell.forTableColumn()
        chatNameColumn.onEditCommit = EventHandler { event ->
            onChangeName(event)
        }

        //creation
        chatCreationColumn.cellValueFactory = PropertyValueFactory<Chat, Timestamp>("creation")
        chatCreationColumn.cellFactory = TextFieldTableCell.forTableColumn<Chat, Timestamp>(
                object : StringConverter<Timestamp>() {
                    override fun toString(obj: Timestamp): String {
                        return SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(obj)
                    }

                    override fun fromString(string: String): Timestamp {
                        return try {
                            java.sql.Timestamp.valueOf(string)
                        } catch (e: IllegalArgumentException) {
                            Alert(Alert.AlertType.ERROR, "Неверный формат даты").show()
                            java.sql.Timestamp.valueOf(LocalDateTime.now())
                        }
                    }
                }
        )
        chatCreationColumn.onEditCommit = EventHandler { event ->
            onChangeCreation(event)
        }

        //owner

        chatOwnerColumn.onEditCommit = EventHandler { event ->
            onChangeOwner(event)
        }

        //deleted
        chatDeletedColumn.cellValueFactory = Callback { param ->
            val chat: Chat = param.value
            val property = SimpleBooleanProperty(chat.delete)
            property.addListener { _, _, newValue ->
                chat.delete = newValue
                saveChangedChat(chat)
            }
            property
        }
        chatDeletedColumn.cellFactory = Callback { param ->
            val cell = CheckBoxTableCell<Chat, Boolean>()
            cell.alignment = Pos.CENTER
            cell
        }

        chatsTable.rowFactory = Callback { param ->
            val row = TableRow<Chat>()
            row.itemProperty().addListener { observable, oldValue, newValue ->
                if (newValue != null) {
                    if (newValue.id <= 0) {
                        row.pseudoClassStateChanged(PseudoClass.getPseudoClass("new"), true)
                    } else {
                        row.pseudoClassStateChanged(PseudoClass.getPseudoClass("new"), false)
                    }
                }
            }
            row
        }
    }

    private fun onChangeName(event: TableColumn.CellEditEvent<Chat, String>) {
        val chat = event.tableView.items[event.tablePosition.row]
        val oldValue = chat.name
        val newValue = event.newValue.trim()
        if (newValue.isBlank()) {
            Alert(Alert.AlertType.ERROR, "Невозможно сохранить чат с пустым именем").show()
            chat.name = oldValue
            chatsTable.refresh()
            return
        }
        chat.name = newValue
        try {
            saveChangedChat(chat)
        } catch (e: SQLException) {
            Alert(Alert.AlertType.ERROR, e.message).show()
            chat.name = oldValue
            chatsTable.refresh()
        }
    }

    private fun onChangeCreation(event: TableColumn.CellEditEvent<Chat, Timestamp>) {
        val chat = event.tableView.items[event.tablePosition.row]
        val oldValue = chat.creation
        chat.creation = event.newValue
        try {
            saveChangedChat(chat)
        } catch (e: SQLException) {
            Alert(Alert.AlertType.ERROR, e.message).show()
            chat.creation = oldValue
            chatsTable.refresh()
        }
    }

    private fun onChangeOwner(event: TableColumn.CellEditEvent<Chat, User>) {
        val chat = event.tableView.items[event.tablePosition.row]
        val oldValue = chat.owner
        chat.owner = event.newValue
        try {
            saveChangedChat(chat)
        } catch (e: SQLException) {
            Alert(Alert.AlertType.ERROR, e.message).show()
            chat.owner = oldValue
            chatsTable.refresh()
        }
    }

    @Throws(SQLException::class)
    private fun saveChangedChat(chat: Chat) {
        if (chat.id == -1L) {
            val sql = "insert into chat (name, creation, owner_id, deleted) values (?, ?, ?, ?)"
            val statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
            statement.setString(1, chat.name)
            statement.setTimestamp(2, chat.creation)
            statement.setLong(3, chat.owner.id)
            statement.setBoolean(4, chat.delete)
            statement.execute()
            val keys = statement.generatedKeys
            if (keys.next()) {
                chat.id = keys.getLong(1)
            }
        } else {
            val sql = "update chat set name = ?, creation = ?, owner_id = ?, deleted = ? where id = ?"
            val statement = connection.prepareStatement(sql)
            statement.setString(1, chat.name)
            statement.setTimestamp(2, chat.creation)
            statement.setLong(3, chat.owner.id)
            statement.setBoolean(4, chat.delete)
            statement.setLong(5, chat.id)
            statement.execute()
        }
        chatsTable.refresh()
    }

    fun reloadChats(chats: ArrayList<Chat>) {
        val usersList = FXCollections.observableArrayList(users)
        chatOwnerColumn.cellValueFactory = Callback { param ->
            val chat: Chat = param.value
            SimpleObjectProperty<User>(chat.owner)
        }
        chatOwnerColumn.cellFactory = ComboBoxTableCell.forTableColumn(usersList)
        chatsTable.items = FXCollections.observableArrayList(chats)
        chatsTable.refresh()
    }

    fun deleteChat(actionEvent: ActionEvent) {
        val chat = chatsTable.selectionModel.selectedItem ?: return
        if (chat.id != -1L) {
            val sql = "delete from chat where id = ?"
            val statement = connection.prepareStatement(sql)
            statement.setLong(1, chat.id)
            try {
                statement.execute()
            } catch (e: SQLException) {
                Alert(Alert.AlertType.ERROR,
                        "Невозможно удалить чат, потому что у него есть зависимые объекты").show()
                return
            }
        }
        chatsTable.items.remove(chat)
        chatsTable.refresh()
    }

    fun addNewChat(actionEvent: ActionEvent) {
        if (users.count() > 0) {
            chatsTable.items.add(Chat(-1L, "Новый чат", Timestamp.valueOf(LocalDateTime.now()),users[0], false))
            chatsTable.refresh()
        } else {
            Alert(Alert.AlertType.ERROR, "Необходимо сначала создать пользователя.").show()
        }
    }

}
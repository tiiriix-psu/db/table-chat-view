package ru.fnight.tablechatview.controllers

import javafx.fxml.FXML
import javafx.scene.control.TabPane
import javafx.scene.layout.AnchorPane
import ru.fnight.tablechatview.Utils
import ru.fnight.tablechatview.domains.Chat
import ru.fnight.tablechatview.domains.Session
import ru.fnight.tablechatview.domains.User

class Window {

    @FXML
    lateinit var tabPane: TabPane
    @FXML
    lateinit var usersTab: AnchorPane
    @FXML
    lateinit var chatsTab: AnchorPane
    @FXML
    lateinit var sessionsTab: AnchorPane
    @FXML
    lateinit var usersTabController: UsersController
    @FXML
    lateinit var chatsTabController: ChatsController
    @FXML
    lateinit var sessionsTabController: SessionsController

    private val connection = Utils.createConnection()
    private val users = ArrayList<User>()
    private val chats = ArrayList<Chat>()
    private val sessions = ArrayList<Session>()

    @FXML
    fun initialize() {
        if (connection == null) {
            return
        }
        usersTabController.connection = connection
        chatsTabController.connection = connection
        sessionsTabController.connection = connection
        chatsTabController.users = users
        sessionsTabController.users = users
        sessionsTabController.chats = chats

        tabPane.selectionModel.selectedItemProperty().addListener { _, _, _ ->
            val selectedTabName = tabPane.selectionModel.selectedItem.text
            when (selectedTabName) {
                "Пользователи" -> {
                    reloadUsers()
                }
                "Чаты" -> {
                    reloadUsers()
                    reloadChats()
                }
                "Сессии" -> {
                    reloadUsers()
                    reloadChats()
                    reloadSessions()
                }
            }
        }
        reloadUsers()
    }

    private fun reloadUsers() {
        if (connection == null) {
            return
        }
        val sql = "select id, login, registration, pwd from users"
        val statement = connection.prepareStatement(sql)
        val resultSet = statement.executeQuery()

        users.clear()

        while (resultSet.next()) {
            val id = resultSet.getLong("id")
            val login = resultSet.getString("login")
            val registration = resultSet.getTimestamp("registration")
            val pwd = resultSet.getString("pwd")
            users.add(User(id, login, registration, pwd))
        }

        usersTabController.reloadUsers(users)
    }

    private fun reloadSessions() {
        if (connection == null) {
            return
        }

        val sql = "select id, chat_id, user_id, start, \"end\" from session"
        val statement = connection.prepareStatement(sql)
        val resultSet = statement.executeQuery()

        sessions.clear()

        while (resultSet.next()) {
            val id = resultSet.getLong("id")
            val chatId = resultSet.getLong("chat_id")
            val userId = resultSet.getLong("user_id")
            val start = resultSet.getTimestamp("start")
            val end = resultSet.getTimestamp("end")
            sessions.add(Session(id, chats.find { chat -> chat.id == chatId }!!,
                    users.find { user -> user.id == userId }!!, start, end))
        }

        sessionsTabController.reloadSessions(sessions)
    }

    private fun reloadChats() {
        if (connection == null) {
            return
        }

        val sql = "select id, name, creation, owner_id, deleted from chat"
        val statement = connection.prepareStatement(sql)
        val resultSet = statement.executeQuery()

        chats.clear()

        while (resultSet.next()) {
            val id = resultSet.getLong("id")
            val name = resultSet.getString("name")
            val creation = resultSet.getTimestamp("creation")
            val ownerId = resultSet.getLong("owner_id")
            val deleted = resultSet.getBoolean("deleted")
            chats.add(Chat(id, name, creation, users.find { user -> user.id == ownerId }!!, deleted))
        }

        chatsTabController.reloadChats(chats)
    }
}
package ru.fnight.tablechatview.controllers

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.FXCollections
import javafx.css.PseudoClass
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.geometry.Pos
import javafx.scene.control.Alert
import javafx.scene.control.TableColumn
import javafx.scene.control.TableRow
import javafx.scene.control.TableView
import javafx.scene.control.cell.CheckBoxTableCell
import javafx.scene.control.cell.ComboBoxTableCell
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.control.cell.TextFieldTableCell
import javafx.util.Callback
import javafx.util.StringConverter
import ru.fnight.tablechatview.domains.Chat
import ru.fnight.tablechatview.domains.Session
import ru.fnight.tablechatview.domains.User
import java.sql.Connection
import java.sql.SQLException
import java.sql.Statement
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.LocalDateTime

class SessionsController {

    lateinit var sessionsTable: TableView<Session>
    lateinit var sessionChatColumn: TableColumn<Session, Chat>
    lateinit var sessionUserColumn: TableColumn<Session, User>
    lateinit var sessionStartColumn: TableColumn<Session, Timestamp>
    lateinit var sessionEndColumn: TableColumn<Session, Timestamp>

    lateinit var connection: Connection
    var users = ArrayList<User>()
    var chats = ArrayList<Chat>()
    @FXML
    private fun initialize() {
        sessionsTable.isEditable = true

        //chat
        sessionChatColumn.onEditCommit = EventHandler { event ->
            onChangeChat(event)
        }

        //user
        sessionUserColumn.onEditCommit = EventHandler { event ->
            onChangeUser(event)
        }

        //start
        sessionStartColumn.cellValueFactory = PropertyValueFactory<Session, Timestamp>("start")
        sessionStartColumn.cellFactory = TextFieldTableCell.forTableColumn<Session, Timestamp>(
                object : StringConverter<Timestamp>() {
                    override fun toString(obj: Timestamp): String {
                        return SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(obj)
                    }

                    override fun fromString(string: String): Timestamp {
                        return try {
                            java.sql.Timestamp.valueOf(string)
                        } catch (e: IllegalArgumentException) {
                            Alert(Alert.AlertType.ERROR, "Неверный формат даты").show()
                            java.sql.Timestamp.valueOf(LocalDateTime.now())
                        }
                    }
                }
        )
        sessionStartColumn.onEditCommit = EventHandler { event ->
            onChangeStart(event)
        }

        //end
        sessionEndColumn.cellValueFactory = PropertyValueFactory<Session, Timestamp>("end")
        sessionEndColumn.cellFactory = TextFieldTableCell.forTableColumn<Session, Timestamp>(
                object : StringConverter<Timestamp>() {
                    override fun toString(obj: Timestamp): String {
                        return SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(obj)
                    }

                    override fun fromString(string: String): Timestamp {
                        return try {
                            java.sql.Timestamp.valueOf(string)
                        } catch (e: IllegalArgumentException) {
                            Alert(Alert.AlertType.ERROR, "Неверный формат даты").show()
                            java.sql.Timestamp.valueOf(LocalDateTime.now())
                        }
                    }
                }
        )
        sessionEndColumn.onEditCommit = EventHandler { event ->
            onChangeEnd(event)
        }

        sessionsTable.rowFactory = Callback { param ->
            val row = TableRow<Session>()
            row.itemProperty().addListener { observable, oldValue, newValue ->
                if (newValue != null) {
                    if (newValue.id <= 0) {
                        row.pseudoClassStateChanged(PseudoClass.getPseudoClass("new"), true)
                    } else {
                        row.pseudoClassStateChanged(PseudoClass.getPseudoClass("new"), false)
                    }
                }
            }
            row
        }
    }

    private fun onChangeChat(event: TableColumn.CellEditEvent<Session, Chat>) {
        val session = event.tableView.items[event.tablePosition.row]
        val oldValue = session.chat
        session.chat = event.newValue
        try {
            saveChangedSession(session)
        } catch (e: SQLException) {
            Alert(Alert.AlertType.ERROR, e.message).show()
            session.chat = oldValue
            sessionsTable.refresh()
        }
    }

    private fun onChangeUser(event: TableColumn.CellEditEvent<Session, User>) {
        val session = event.tableView.items[event.tablePosition.row]
        val oldValue = session.user
        session.user = event.newValue
        try {
            saveChangedSession(session)
        } catch (e: SQLException) {
            Alert(Alert.AlertType.ERROR, e.message).show()
            session.user = oldValue
            sessionsTable.refresh()
        }
    }

    private fun onChangeStart(event: TableColumn.CellEditEvent<Session, Timestamp>) {
        val session = event.tableView.items[event.tablePosition.row]
        val oldValue = session.start
        session.start = event.newValue
        try {
            saveChangedSession(session)
        } catch (e: SQLException) {
            Alert(Alert.AlertType.ERROR, e.message).show()
            session.start = oldValue
            sessionsTable.refresh()
        }
    }

    private fun onChangeEnd(event: TableColumn.CellEditEvent<Session, Timestamp>) {
        val session = event.tableView.items[event.tablePosition.row]
        val oldValue = session.end
        session.end = event.newValue
        try {
            saveChangedSession(session)
        } catch (e: SQLException) {
            Alert(Alert.AlertType.ERROR, e.message).show()
            session.end = oldValue
            sessionsTable.refresh()
        }
    }

    @Throws(SQLException::class)
    private fun saveChangedSession(session: Session) {
        if (session.id == -1L) {
            val sql = "insert into session (chat_id, user_id, start,\"end\") values (?, ?, ?, ?)"
            val statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
            statement.setLong(1, session.chat.id)
            statement.setLong(2, session.user.id)
            statement.setTimestamp(3, session.start)
            statement.setTimestamp(4,  session.end)
            statement.execute()
            val keys = statement.generatedKeys
            if (keys.next()) {
                session.id = keys.getLong(1)
            }
        } else {
            val sql = "update session set chat_id = ?, user_id = ?, start = ?, \"end\" = ? where id = ?"
            val statement = connection.prepareStatement(sql)
            statement.setLong(1, session.chat.id)
            statement.setLong(2, session.user.id)
            statement.setTimestamp(3, session.start)
            statement.setTimestamp(4, session.end)
            statement.setLong(5, session.id)
            statement.execute()
        }
    }

    fun reloadSessions(sessions: ArrayList<Session>) {
        val usersList = FXCollections.observableArrayList(users)
        val chatsList = FXCollections.observableArrayList(chats)
        sessionUserColumn.cellValueFactory = Callback { param ->
            val session: Session = param.value
            SimpleObjectProperty<User>(session.user)
        }
        sessionUserColumn.cellFactory = ComboBoxTableCell.forTableColumn(usersList)
        sessionChatColumn.cellValueFactory = Callback { param ->
            val session: Session = param.value
            SimpleObjectProperty<Chat>(session.chat)
        }
        sessionChatColumn.cellFactory = ComboBoxTableCell.forTableColumn(chatsList)
        sessionsTable.items = FXCollections.observableArrayList(sessions)
        sessionsTable.refresh()
    }


    fun deleteSession(actionEvent: ActionEvent) {
        val session = sessionsTable.selectionModel.selectedItem ?: return
        if (session.id != -1L) {
            val sql = "delete from session where id = ?"
            val statement = connection.prepareStatement(sql)
            statement.setLong(1, session.id)
            try {
                statement.execute()
            } catch (e: SQLException) {
                Alert(Alert.AlertType.ERROR,
                        "Невозможно удалить сессию, потому что у него есть зависимые объекты").show()
                return
            }
        }
        sessionsTable.items.remove(session)
        sessionsTable.refresh()
    }

    fun addNewSession(actionEvent: ActionEvent) {
        if (chats.count() > 0) {
            val start = Timestamp.valueOf(LocalDateTime.now())
            val end = Timestamp.valueOf(LocalDateTime.now().plusHours(2))
            sessionsTable.items.add(Session(-1L, chats[0], users[0], start, end))
            sessionsTable.refresh()
        } else {
            Alert(Alert.AlertType.ERROR, "Необходимо сначала создать чаты.").show()
        }
    }

}
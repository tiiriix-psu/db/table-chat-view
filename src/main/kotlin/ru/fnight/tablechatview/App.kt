package ru.fnight.tablechatview

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage
import ru.fnight.tablechatview.controllers.Window

class App : Application() {
    override fun start(stage: Stage) {
        val fxmlFile = "/fxml/window.fxml"
        val loader = FXMLLoader(javaClass.getResource(fxmlFile))
        val root = loader.load<Parent>()
        stage.title = "Редактирование в таблице"
        stage.scene = Scene(root)
        val controller = loader.getController<Window>()
        stage.show()
    }
}
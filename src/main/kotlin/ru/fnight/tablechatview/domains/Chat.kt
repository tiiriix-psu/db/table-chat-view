package ru.fnight.tablechatview.domains

import java.sql.Timestamp

class Chat (var id: Long, var name: String, var creation: Timestamp, var owner: User, var delete: Boolean) {
    override fun toString(): String {
        return name
    }
}
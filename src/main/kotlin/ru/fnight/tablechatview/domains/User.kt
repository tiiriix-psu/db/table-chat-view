package ru.fnight.tablechatview.domains

import java.sql.Timestamp

class User(var id: Long, var login: String, var registration: Timestamp, var pwd: String) {
    override fun toString(): String {
        return login
    }
}
package ru.fnight.tablechatview.domains

import java.sql.Timestamp

class Session (var id: Long, var chat: Chat, var user: User, var start: Timestamp, var end: Timestamp)
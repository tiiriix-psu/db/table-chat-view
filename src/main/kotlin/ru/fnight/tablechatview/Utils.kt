package ru.fnight.tablechatview

import java.io.File
import java.io.FileReader
import java.io.IOException
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.util.*

class Utils {
    companion object {
        fun createConnection(): Connection? {
            val file = File("database.properties")

            val properties = Properties()
            try {
                properties.load(FileReader(file))
            } catch (e: IOException) {
                e.printStackTrace()
            }

            val host = properties.getProperty("host")
            val port = properties.getProperty("port")
            val user = properties.getProperty("user")
            val database = properties.getProperty("database")
            val password = properties.getProperty("password")

            val url = String.format("jdbc:postgresql://%s:%s/%s", host, port, database)
            return try {
                DriverManager.getConnection(url, user, password)
            } catch (e: SQLException) {
                e.printStackTrace()
                null
            }

        }
    }

}